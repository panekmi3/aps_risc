module ALU( 
    input [3:0] alu_control,
    input signed [31:0] A,
    input signed [31:0] B,

    output reg [31:0] result,
    output reg zero
    );
    
    always @(*) begin
        case(alu_control)
            0 : result = A + B; //add
            1 : result = A - B; //sub
            2 : result = A << B; //sll
            3 : result = A & B; //and
            4 : //slt
                begin 
                    if (A < B)
                        result = 1;
                    else
                        result = 0;
                end
            default : result = 0;
            5 : result = A >>> B; //sra
            6 : //adduqb
                begin
                result[31:24] = A[31:24] + B[31:24];
                result[23:16] = A[23:16] + B[23:16];
                result[15:8] = A[15:8] + B[15:8];
                result[7:0] = A[7:0] + B[7:0];
                end
            7 : result = B; //lui
            8 : result = A >> B; //srl
            9 : result = A | B;
        endcase
    end

    always @(*) begin
    if (result == 0)
        zero = 1;
    else
        zero = 0;
    end

endmodule

module Adder(
    input [31:0] A,
    input [31:0] B,
    output reg [31:0] result
    );
    
    always @(*) begin
        result = A + B;
    end

endmodule

module add4(
    input [31:0] A,
    output reg [31:0] result
    );

    always @(*) begin
        result = A + 4;
    end

endmodule

module multiplexor(
    input [31:0] A, //0
    input [31:0] B, //1
    input select,
    output reg [31:0] out
    );

    always @(*) begin
        if (select == 0)
            out = A;
        else
            out = B;
    end
endmodule

module threeInMultiplexor(
        input [31:0] A, //00
        input [31:0] B, //01
        input [31:0] C, //10
        input select0,
        input select1,
        output reg [31:0] out
    );

    always @(*) begin
        case ( { select1, select0 })
            0 : out = A;
            1 : out = B;
            2 : out = C;
            default : out = 0;
        endcase
    end

endmodule

module controller(
    input [31:0] instruction,
    output reg [7:0] controllWord,
    /*  7 - ALUSrcA
        6 - BranchJalr
        5 - BranchJal
        4 - RegWrite
        3 - BranchBeq
        2 - MemToReg
        1 - MemWrite
        0 - ALUSrcB */
    output reg [3:0] ALUControl
    );
    
    reg[7:0] ALUSrcA         = 8'b10000000;
    reg[7:0] BranchJalr      = 8'b01000000;
    reg[7:0] BranchJal       = 8'b00100000;
    reg[7:0] RegWrite        = 8'b00010000;
    reg[7:0] BranchBeq       = 8'b00001000;
    reg[7:0] MemToReg        = 8'b00000100;
    reg[7:0] MemWrite        = 8'b00000010;
    reg[7:0] ALUSrcB         = 8'b00000001;

    always @ (*)
    begin
        case ( instruction[6:0] )
            7'b0110011 : //add, and, sub, slt, sll, srl, sra, or
            begin
                controllWord = RegWrite;
                case ( { instruction[14:12], instruction[31:25] } )
                    10'b000_0000000 : ALUControl = 0; //add
                    10'b000_0100000 : ALUControl = 1; //sub
                    10'b010_0000000 : ALUControl = 4; //slt
                    10'b111_0000000 : ALUControl = 3; //and
                    10'b001_0000000 : ALUControl = 2; //sll
                    10'b101_0000000 : ALUControl = 8; //srl
                    10'b101_0100000 : ALUControl = 5; //sra
                    10'b110_0000000 : ALUControl = 9; //or
                    default: ALUControl = 0;
                endcase
            end

            7'b0010011 : begin controllWord = RegWrite | ALUSrcB;                   ALUControl = 0; end //addi
            7'b0001011 : begin controllWord = RegWrite;                             ALUControl = 6; end //adduqb
            7'b1100011 : begin controllWord = BranchBeq;                            ALUControl = 1; end //beq
            7'b0000011 : begin controllWord = RegWrite | MemToReg | ALUSrcB;        ALUControl = 0; end //lw
            7'b0100011 : begin controllWord = MemWrite | ALUSrcB;                   ALUControl = 0; end //sw
            7'b0110111 : begin controllWord = RegWrite | ALUSrcB;                   ALUControl = 7; end //lui
            7'b1101111 : begin controllWord = RegWrite | BranchJal;                 ALUControl = 0; end //jal
            7'b1100111 : begin controllWord = RegWrite | BranchJalr | ALUSrcB;      ALUControl = 0; end //jalr
            7'b0010111 : begin controllWord = RegWrite | ALUSrcA | ALUSrcB;         ALUControl = 0; end //auipc
            default    : begin controllWord = 0;                                    ALUControl = 0; end
        endcase
    end

endmodule

module register(
    input[31:0] nextValue,
    output reg [31:0] value,
    input clk,reset
    );

    always @(posedge clk) begin
        if (reset)
            value = 0;
        else
            value = nextValue;
        
    end

endmodule

module immDecode(
    input[31:0] instruction,
    output reg [31:0] operand
    );

    always @ (*)
    case ( instruction[6:0] )
        7'b0110011 : operand = 0; //add, and, sub, slt, sll, srl, sra
        7'b0010011 : operand = { {20{instruction[31]}}, instruction[31:20] }; //addi
        7'b0001011 : operand = 0; // adduqb
        7'b1100011 : operand = { {20{instruction[31]}}, instruction[31], instruction[7], instruction[30:25], instruction[11:8], 1'b0 }; //beq
        7'b0000011 : operand = { {20{instruction[31]}}, instruction[31:20] }; //lw
        7'b0100011 : operand = { {20{instruction[31]}}, instruction[31:25], instruction[11:7] }; //sw
        7'b0110111 : operand = { instruction[31:12], 12'b000000000000}; //lui
        7'b1101111 : operand = { {12{ instruction[31]}}, instruction[31], instruction[19:12], instruction[20], instruction[31:21], 1'b0 }; //jal
        7'b1100111 : operand = { {20{instruction[31]}}, instruction[31:20] }; // jalr
        7'b0010111 : operand = { instruction[31:12], 12'b000000000000}; //auipc
        default : operand = 0 ;
    endcase 

endmodule

module registerSet(
    input[4:0] A1, A2, A3,
    input[31:0] WD3,
    input clk,
    input WE3,
    output reg[31:0] RD1, RD2
    );

    reg[31:0] registers[31:0];

    always @(*) begin
        registers[0] = 0;
        RD1 = registers[A1];
        RD2 = registers[A2];
    end

    always @(posedge clk) begin
        if (WE3) begin
            registers[A3] = WD3;   
        end
    end

endmodule

module AndGate(
    input A, B,
    output C
    );

    assign C = ( A & B );

endmodule

module OrGate(
    input A, B,
    output C
    );

    assign C = ( A | B );

endmodule

module processor( 
        input         clk, reset,
        output [31:0] PC,
        input  [31:0] instruction,
        output        WE,
        output [31:0] address_to_mem,   //ALUOUT
        output [31:0] data_to_mem,      //RD2
        input  [31:0] data_from_mem
        );  


        wire [31:0] ALUInA, ALUInB, DataToReg, PCNext, ImmOp, JumpPC, NextInstructionPC, lolxd, RD1;
        wire Zero, BranchJalr, BranchJal, RegWrite, BranchBeq, MemToReg, ALUSrcA, ALUSrcB, OrGate0Out, AndGate0Out, OrGate1Out;
        wire [3:0] ALUControl;
        
        ALU alu_inst( ALUControl, ALUInA, ALUInB, address_to_mem, Zero);
        registerSet GPRset( instruction[19:15], instruction[24:20], instruction[11:7], DataToReg, clk, RegWrite, RD1, data_to_mem );
        register ProgramCounter( PCNext, PC, clk, reset );
        immDecode ImmedateDecoder( instruction, ImmOp );
        controller ControlUnit( instruction, {ALUSrcA, BranchJalr, BranchJal, RegWrite, BranchBeq, MemToReg, WE, ALUSrcB}, ALUControl );
        Adder JumpAdder( ImmOp, PC, JumpPC );
        add4 NextInstructionAdder( PC, NextInstructionPC );

        threeInMultiplexor NextPCMultiplexor( NextInstructionPC, address_to_mem, JumpPC, BranchJalr, OrGate0Out, PCNext );


        multiplexor ALUSourceMultiplexor( data_to_mem, ImmOp, ALUSrcB, ALUInB );
        multiplexor BranchMultiplexor( address_to_mem, NextInstructionPC, OrGate1Out, lolxd);
        multiplexor DataSelectMultiplexor( lolxd, data_from_mem, MemToReg, DataToReg );
        multiplexor ALUSourceAMultiplexor( RD1, PC, ALUSrcA, ALUInA );

        AndGate AndGate0( Zero, BranchBeq, AndGate0Out );
        OrGate OrGate0( AndGate0Out, BranchJal, OrGate0Out );
        OrGate OrGate1( BranchJal, BranchJalr, OrGate1Out );

endmodule