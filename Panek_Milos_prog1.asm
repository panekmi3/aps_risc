.data                  # directive indicating start of the data segment
.align  2              # set data alignment to 4 bytes

.word

.text                  # beginning of the text segment (or code segment)
j main                 # jump to main

merge:
	lui t1, 0x53504
	addi t1, t1, 0x12e
	sw t1, 0(a2) # ulozi .APS do noveho obrazku
	
	lw t2, 4(a1)  # sirka
	lw t3, 8(a1)  # vyska
	sw t2, 4(a2)
	sw t3, 8(a2)
	
	addi t0, zero, 0  #  pocet pixelu
	
	mult_loop:
		add t0, t0, t2
		addi t3, t3, -1
		beq t3, zero, mult_done
		j mult_loop 
	mult_done:
	
	# v t0 je ted pocet pixelu
	addi t1, zero, 0 #pozice
	
	loop_pixels:
		
		lw t2, 12(a0) #pixel prvniho orazku
		lw t3, 12(a1) #pixel druheho obrazku
		
		add t4, t2, t3 #secti pixel data
		lui t2, 0xff000 
		or t4, t4, t2 #FF do alfa kanalu
		
		sw t4, 12(a2) #ulo� pixel do noveho obrazku
		
		addi t1, t1, 1
		beq t1, t0, done_pixels
		
		addi a0, a0, 4
		addi a1, a1, 4
		addi a2, a2, 4
		j loop_pixels
	done_pixels:
	
	mv a0, t0
	
	ret

main:
	lw a0, 0x00000004
	lw a1, 0x00000008
	lw a2, 0x0000000c
	
	jal merge
	
	sw a0, 0x00000010
