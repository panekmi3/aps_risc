.data                  # directive indicating start of the data segment
.align  2              # set data alignment to 4 bytes


.text                  # beginning of the text segment (or code segment)
j main                 # jump to main

main:
  addi t1, zero, 1
  addi t2, zero, 31
  sll t1, t1, t2
  sw t1, 100
  sra t1, t1, t2
  sw t1, 104
  addi t2, zero, 15
  sll t1, t1, t2
  srl t1, t1, t2
  sw t1, 108


  end_loop:
  	j end_loop